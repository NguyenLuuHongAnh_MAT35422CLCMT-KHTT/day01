CREATE DATABASE IF NOT EXISTS ltweb;

-- CSDL ltweb
USE ltweb;
CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,

    name VARCHAR(255) NOT NULL, 

    gender VARCHAR(10) NOT NULL,

    department VARCHAR(255) NOT NULL,

    birthdate  DATE NOT NULL,

    address TEXT NOT NULL,
    
    image VARCHAR(255) 
);
