<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<?php
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" enctype="multipart/form-data"
        action = "savetodata.php">
        <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="name">Họ và
                    tên</div>

                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['name'];
                    ?>
                </div>
                <input type = "hidden" name = "name" value = "<?php echo $_POST['name'];?>">
        </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20 " for="gender">
                    Giới tính</div>
                <div id="gender" name="gender" class="fl-1 p-10-20">
                        <?php
                        echo $_POST['gender'];
                        ?>
                </div>
                <input type = "hidden" name = "gender" value = "<?php echo $_POST['gender'];?>">
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="department">
                    Phân khoa</div>
                <div class="fl-1 p-10-20">
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                    echo $departments[$_POST['department']];
                    // echo "<pre>";
                    // echo print_r($_POST);
                    ?>
                </div>
                <input type = "hidden" name = "department" value = "<?php echo $_POST['department'];?>">
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20 w-170"
                    for="birthdate">Ngày sinh</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['birthdate'];
                    ?>
                <input type = "hidden" name = "birthdate" value = "<?php echo $_POST['birthdate'];?>">   
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 w-170"
                    for="address">Địa chỉ</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['address'];
                    ?>
                </div>
                <input type = "hidden" name = "address" value = "<?php echo $_POST['address'];?>">
            </div>

            <div class="form-group" style="align-items: unset">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20"
                    for="profileImage" style="height: 20%">Hình ảnh</div>
                <div class="p-0-20 w-30">
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["image"])) {
                    $uploadDir = "image/"; 
                    $uploadFile = $uploadDir . basename($_FILES["image"]["name"]);
                
                    $imageFileType = strtolower(pathinfo($uploadFile, PATHINFO_EXTENSION));
                    $allowedExtensions = array("jpg", "jpeg", "png", "gif");
                
                    if (in_array($imageFileType, $allowedExtensions)) {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $uploadFile)) {
                            echo '<img src="' . $uploadFile . '" alt="Uploaded Image" width = "100px" height = "auto">';
                        } else {
                            echo "Có lỗi xảy ra khi tải lên tệp.";
                        }
                    } else {
                        echo "Vui lòng tải lên tệp hình ảnh có định dạng JPG, JPEG, PNG hoặc GIF.";
                    }
                }
                ?>

                </div>
                <input type = "hidden" name = "image" value = "<?php echo basename($_FILES["profileImage"]["name"]);?>">
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>
